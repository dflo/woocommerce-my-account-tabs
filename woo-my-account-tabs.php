<?php
/**
 * @since             1.0.0
 * @package           Woo_My_Account_Tabs
 *
 * @wordpress-plugin
 * Plugin Name:      Woocommerce My Account Tabs
 * Plugin URI:        https://gitlab.com/dflo/woocommerce-my-account-tabs
 * Description:       Creates new custom tabs on My Account page on a Woocommerce site.
 * Version:           1.0.0
 * Author:            Flora Dsouza
 * Author URI:        https://gitlab.com/dflo
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-my-account-tabs
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'WOOCOMMERCE_MY_ACCOUNT_TABS_VERSION', '1.0.0' );

/**
 * Plugin Name.
 */
define( 'WMAT_PLUGIN_NAME', 'woocommerce-my-account-tabs' );

/**
 * Define plugin directory.
 */
if ( ! defined( 'WMAT_PLUGIN_DIR' ) ) {
	define( 'WMAT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

/**
 * Define plugin file.
 */
if ( ! defined( 'WMAT_PLUGIN_FILE' ) ) {
	define( 'WMAT_PLUGIN_FILE', __FILE__ );
}

/**
 * Define plugin URL.
 */
if ( ! defined( 'WMAT_PLUGIN_URL' ) ) {
	define( 'WMAT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-my-account-tabs.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_storis_sync() {
	return Woo_My_Account_Tabs::instance();
}

run_woocommerce_storis_sync();
