<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      1.0.0
 *
 * @package    Woo_My_Account_Tabs
 * @subpackage Woo_My_Account_Tabs/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woo_My_Account_Tabs
 * @subpackage Woo_My_Account_Tabs/includes
 */
class Woo_My_Account_Tabs {
	/**
	 * The single instance of the class.
	 *
	 * @var Woo_My_Account_Tabs
	 * @access   protected
	 * @since 1.0.0
	 */
	protected static $instance = null;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_MY_ACCOUNT_TABS_VERSION' ) ) {
			$this->version = WOOCOMMERCE_MY_ACCOUNT_TABS_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-my-account-tabs';

		$this->run();
	}

	/**
	 * Main Woo_My_Account_Tabs instance. Ensures only one instance of Woo_My_Account_Tabs is loaded or can be loaded.
	 *
	 * @static
	 * @return Woo_My_Account_Tabs
	 * @since  1.0.0
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Runs the execution of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->load_dependencies();

		My_Account_Tab::instance();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The my-account files.
		 */
		require_once WMAT_PLUGIN_DIR . 'includes/class-my-account-tab.php';
	}
}
