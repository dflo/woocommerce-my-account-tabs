<?php
/**
 * Create Tab on My Account page.
 *
 * @since      1.0.0
 *
 * @package    Woo_My_Account_Tabs
 * @subpackage Woo_My_Account_Tabs/includes
 */

defined( 'ABSPATH' ) || exit;

/**
 * Adds a tab to My Account Page.
 *
 * @since      1.0.0
 * @package    Woo_My_Account_Tabs
 * @subpackage Woo_My_Account_Tabs/includes
 */
class My_Account_Tab {
	/**
	 * The single instance of the class.
	 *
	 * @var My_Account_Tab
	 * @access   protected
	 * @since 1.0.0
	 */
	protected static $instance = null;

	/**
	 * All tabs.
	 *
	 * @var array
	 */
	protected $tabs = array();

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->initialize_tabs();

		add_action( 'init', array( $this, 'register_my_account_tab_post_type' ) );
		add_action( 'init', array( $this, 'add_my_account_tab_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'my_account_tab_query_vars' ) );
		add_filter( 'woocommerce_account_menu_items', array( $this, 'add_link_my_account' ) );

		$this->add_endpoint_content();
	}

	/**
	 * Main My_Account_Tab instance. Ensures only one instance of My_Account_Tab is loaded or can be loaded.
	 *
	 * @static
	 * @return My_Account_Tab
	 * @since  1.0.0
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Register the post type as my_acount_tab.
	 *
	 * @since  1.0.0
	 *
	 * @return void
	 */
	public function register_my_account_tab_post_type() {
		register_post_type(
			'wmat_tab',
			array(
				'labels'              => array(
					'name'                  => __( 'My Account Tabs', 'woocommerce-my-account-tabs' ),
					'singular_name'         => __( 'My Account Tab', 'woocommerce-my-account-tabs' ),
					'menu_name'             => _x( 'My Account Tabs', 'Admin menu name', 'woocommerce-my-account-tabs' ),
					'add_new'               => __( 'Add tab', 'woocommerce-my-account-tabs' ),
					'add_new_item'          => __( 'Add new tab', 'woocommerce-my-account-tabs' ),
					'edit'                  => __( 'Edit', 'woocommerce-my-account-tabs' ),
					'edit_item'             => __( 'Edit tab', 'woocommerce-my-account-tabs' ),
					'new_item'              => __( 'New tab', 'woocommerce-my-account-tabs' ),
					'view_item'             => __( 'View tab', 'woocommerce-my-account-tabs' ),
					'search_items'          => __( 'Search my account tabs', 'woocommerce-my-account-tabs' ),
					'not_found'             => __( 'No tabs found', 'woocommerce-my-account-tabs' ),
					'not_found_in_trash'    => __( 'No tabs found in trash', 'woocommerce-my-account-tabs' ),
					'parent'                => __( 'Parent tab', 'woocommerce-my-account-tabs' ),
					'filter_items_list'     => __( 'Filter tabs', 'woocommerce-my-account-tabs' ),
					'items_list_navigation' => __( 'Tabs navigation', 'woocommerce-my-account-tabs' ),
					'items_list'            => __( 'Tabs list', 'woocommerce-my-account-tabs' ),
				),
				'description'         => __( 'This is where you can add new tabs that appear on My Account Page.', 'woocommerce-my-account-tabs' ),
				'public'              => false,
				'show_ui'             => true,
				// 'capability_type'     => 'wmat_tab',
				'map_meta_cap'        => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'show_in_menu'        => current_user_can( 'edit_others_shop_orders' ) ? 'woocommerce' : true,
				'hierarchical'        => false,
				'rewrite'             => false,
				'query_var'           => false,
				'supports'            => array( 'title', 'editor', 'publicize', 'wpcom-markdown' ),
				'show_in_nav_menus'   => false,
				'show_in_admin_bar'   => true,
			)
		);
	}

	/**
	 * Add the end-points for all my account tabs.
	 *
	 * @since  1.0.0
	 *
	 * @return void
	 */
	public function add_my_account_tab_endpoints() {
		foreach ( $this->tabs as $key => $post ) {
			add_rewrite_endpoint( get_post_field( 'post_name', $post ), EP_ROOT | EP_PAGES );
			flush_rewrite_rules();
		}
	}

	/**
	 * Add the end-point for all my account tabs to query_vars hook.
	 *
	 * @param array $vars vars array.
	 * @return array
	 */
	public function my_account_tab_query_vars( $vars ) {
		foreach ( $this->tabs as $key => $post ) {
			$vars[] = get_post_field( 'post_name', $post );
		}

		return $vars;
	}

	/**
	 * Insert the new end-point for all my account tabs into the My Account menu.
	 *
	 * @param array $items menu items.
	 * @return array
	 */
	public function add_link_my_account( $items ) {
		foreach ( $this->tabs as $key => $post ) {
			if ( current_user_can( 'edit_posts' ) ) {
				$items[ get_post_field( 'post_name', $post ) ] = get_post_field( 'post_title', $post );
			}
		}

		return $items;
	}

	/**
	 * Add content to my account tab endpoints.
	 *
	 * @since  1.0.0
	 *
	 * @return void
	 */
	public function add_endpoint_content() {
		foreach ( $this->tabs as $key => $post ) {
			add_action(
				'woocommerce_account_' . get_post_field( 'post_name', $post ) . '_endpoint',
				function() use ( $post ) {
					echo esc_html( get_post_field( 'post_content', $post ) );
				}
			);
		}
	}

	/**
	 * Get all tabs and store in tabs variable.
	 *
	 * @since  1.0.0
	 *
	 * @return void
	 */
	public function initialize_tabs() {
		$posts = get_posts(
			array(
				'post_type'   => 'wmat_tab',
				'numberposts' => -1,
			)
		);

		$this->tabs = $posts;
	}
}
